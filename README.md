﻿# passwordk

#### 介绍
一款小程序的完整支付输入密码插件《简化版》

#### 软件架构
软件架构说明


#### 安装教程

1.  将《passwordk》包复制到项目里面
2.  在你所需要的页面《xxx.json》里面配置引入下
    "usingComponents": {
        "passwordk": "../../utils/passwordk/passwordk"
    },
3.  在《xxx.wxml》中
    <passwordk show="{{show}}" stest="请输入房间密码" bind:paymentcall="paymentcall" bind:paymentcall="paymentcall"> </passwordk>

#### 使用说明

1.  show  类型为Boolean  显示和关闭
2.  paymentcall  是支付窗口关闭时触发  
3.  paymentcall  是支付密码输入完时触发 

#### 参与贡献

1.  zlk888888zlk  wx

