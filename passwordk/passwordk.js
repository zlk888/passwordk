Component({
  /**
   * 组件的属性列表
   */
  properties: {
    show: Boolean,
    stest: { 
      type: String,
      value: '请输入支付密码'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    pwdVal: '', //输入的密码
    payFocus: true, //文本框焦点,
    stest: '请输入支付密码'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 获取焦点
     */
    getFocus: function() {
      this.setData({
        payFocus: true
      });
    },
    /**
     * 输入密码监听
     */
    inputPwd: function(e) {
      this.setData({
        pwdVal: e.detail.value
      });
      if (e.detail.value.length >= 6) {
        this.triggerEvent('paymentcall', this.data.pwdVal);
        this.setData({
          pwdVal:'',
          show: false
        })
      }
    },
    /**
     * 关闭支付窗口
     */
    closepaywd: function (e) {
      this.setData({
        show:false
      })
      this.triggerEvent('closepaywd', {})
    },
  }
})